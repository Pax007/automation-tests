Feature: Registration process

  Background:
    Given Home page is opened
    When User clicks Sign in link in top menu

  Scenario: Authentication page visibility
    Then Authentication page is opened


  Scenario Outline: Invalid email address format
    And User fill email address field with "<email>"
    And User clicks Create an account button
    Then User gets a message "<expectedMessage>"
    Examples:
      | email             | expectedMessage                                                                                                     |
      | wp@wp             | Invalid email address.                                                                                              |
      | test.test@test.com|  Newsletter : This email address is already registered.|
      |                   | Invalid email address.                                                                                              |
