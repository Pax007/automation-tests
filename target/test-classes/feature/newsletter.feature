Feature: Sign up for the newsletter

  Background:
    Given Home page is opened
    When User click newsletter field

  Scenario: Home page visibility
    Then User is on home page

  Scenario Outline: Invalid email address for newsletter
    And User fill newsletter email address field with "<email>"
    And User clicks Enter
    Then User gets a information "<expectedMessage>"
    Examples:
      | email             | expectedMessage                                                                                                     |
      | wp@wp             | Newsletter : Invalid email address.                   |
      |                   | Newsletter : Invalid email address.                   |


  Scenario: Correct email address for newsletter
    And User fill email address field with correct "email"
    And User clicks Enter
    Then User gets a information "expectedMessage" about success