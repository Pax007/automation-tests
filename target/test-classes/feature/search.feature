Feature: Search

  Scenario: Search with incorrect data
    Given Home page is opened
    When User is on home page
    And User click search button
    Then User gets a information on search site "Please enter a search keyword"