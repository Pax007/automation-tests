//Feature: Go to basket from mine site

  Scenario: Go to basket
    Given Home page is opened
    When User clicks Dresses in menu
    And In filters user chooses Summer Dresses
    And User chooses one on filtered products
    And User change Quantity on 3
    And User change Size on L
    And User change color on different
    And User clicks Add to cart
    Then User is in the cart
