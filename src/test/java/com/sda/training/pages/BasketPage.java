package com.sda.training.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasketPage extends BasePage{

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Proceed to checkout")
    WebElement makeOrder;
    @FindBy (how = How.XPATH, using = "//*[@id=\"header_logo\"]/a/img")
    WebElement logo;

    public void makeOrderIsVisible(){
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(makeOrder));
    }
    public void clickLogo(){
        logo.click();
    }
}