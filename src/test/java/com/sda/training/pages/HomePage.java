package com.sda.training.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    private static String HOMEPAGE_URL = "http://automationpractice.com/index.php";


    @FindBy(how = How.ID, using = "homepage-slider")
    WebElement homepageSlider;

    @FindBy(how = How.LINK_TEXT, using = "Sign in")
    WebElement signInLink;

    @FindBy(how = How.LINK_TEXT, using = "DRESSES")
    WebElement dressesLink;

    @FindBy(how = How.ID, using = "newsletter-input")
    WebElement newsletterInput;

    @FindBy(how = How.CLASS_NAME, using = "blockbestsellers")
    WebElement bestSellersButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"columns\"]/p")
    WebElement newsletterEmailError;

    @FindBy(how = How.ID_OR_NAME, using = "submit_search")
    WebElement searchButton;

    @FindBy(how = How.CSS, using = "#homefeatured > .ajax_block_product:nth-child(2) .button:nth-child(1) > span")
    WebElement toCart;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Proceed to checkout")
    WebElement toCheckout;

    public void openHomePage() {
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(homepageSlider));
    }

    public void clickSignInLink() {

        signInLink.click();
    }

    public void clickDressesLink() throws InterruptedException {
        dressesLink.click();
    }

    public void newsletterFieldClickAndClear(){
        newsletterInput.click();
        newsletterInput.clear();
    }

    public void bestSellersButtonIsClickable(){
        new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(bestSellersButton));
    }

    public void enterNewsletterEmail(String emailAddress){
        newsletterInput.sendKeys(emailAddress);
    }

    public void enterClick(){
        newsletterInput.sendKeys(Keys.ENTER);
    }

    public void waitForErrorMessage() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(newsletterEmailError));
    }

    public String getErrorInfo(){
        return newsletterEmailError.getText();
    }

    public void clickSearchButton(){
        searchButton.click();
    }

    public void toCartFromMineSite(){
        toCart.click();
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(toCheckout));
        toCheckout.click();
    }
    public void enterCorrectNewsletterEmail(String correctEmailAddressForNews){
        newsletterInput.sendKeys(correctEmailAddressForNews);
    }
}
