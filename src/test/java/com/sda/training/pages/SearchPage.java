package com.sda.training.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage extends BasePage{

    @FindBy(how = How.XPATH, using = "//*[@id=\"center_column\"]/p")
    WebElement errorSearchResult;

    public void WaitForResultWithoutResult(){
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(errorSearchResult));
    }

    public String GetResultWithoutResult(){
        return errorSearchResult.getText();
    }
}
