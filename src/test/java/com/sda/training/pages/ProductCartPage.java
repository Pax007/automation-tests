package com.sda.training.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductCartPage extends BasePage{
    @FindBy(how = How.ID_OR_NAME, using = "qty")
    WebElement QtyField;

    @FindBy(how =How.ID, using = "group_1")
    WebElement size;

    @FindBy(how = How.ID, using = "color_8")
    WebElement color;

    @FindBy(how = How.ID_OR_NAME, using = "Submit")
    WebElement toCart;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Proceed to checkout")
    WebElement toCheckout;

    public void setQty(int amountOfItems){
        new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(QtyField));
        QtyField.click();
        QtyField.clear();
        QtyField.sendKeys(String.valueOf(amountOfItems));
    }
    public void setSize(){
        size.click();
        size.sendKeys(Keys.ARROW_DOWN);
        size.sendKeys(Keys.ARROW_DOWN);
        size.sendKeys((Keys.ENTER));
    }

    public void setColor(){
        color.click();
    }

    public void toCart(){
        toCart.click();
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(toCheckout));
        toCheckout.click();
    }
}
