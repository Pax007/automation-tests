package com.sda.training.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DressesListingPage extends BasePage{

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Summer Dresses")
    WebElement summerDressesFilter;

    @FindBy(how = How.CSS, using = ".ajax_block_product:nth-child(2) .button:nth-child(2) > span")
    WebElement moreButton;



    public void setSummerDressesFilter() {
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(summerDressesFilter));
        summerDressesFilter.click();
    }

    public void chooseProduct() {
        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(moreButton));
        moreButton.click();
    }
}
