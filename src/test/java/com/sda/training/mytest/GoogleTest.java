package com.sda.training.mytest;

import com.sda.training.DriverManager;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleTest extends DriverManager {

    @Test
    public void openBrowserTest() throws InterruptedException {
        startDriver();
        DRIVER.get("https://www.google.pl/");
        Thread.sleep(5000);
        destroyDriver();
    }

    @Test
    public void searchInGoogle() throws InterruptedException {
        startDriver();
        DRIVER.get("https://www.google.pl/");
        Thread.sleep(5000);
        DRIVER.switchTo().frame(0);
        WebElement popupAgreeButton = DRIVER.findElement(By.id("introAgreeButton"));
        popupAgreeButton.click();
        DRIVER.switchTo().defaultContent();
        WebElement searchField = DRIVER.findElement(By.name("q"));
        searchField.sendKeys("SDA Academy");
        searchField.sendKeys(Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(DRIVER, 10);
        WebElement searchResults = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("rcnt")));
        Thread.sleep(5000);
        destroyDriver();
    }

    @Test
    public void enterPlayListing() throws InterruptedException{
        startDriver();
        DRIVER.get("https://www.play.pl/");
        Thread.sleep(1000);
        WebElement urzadzeniaButton = DRIVER.findElement(By.cssSelector(".nav-list:nth-child(2) > .nav-item:nth-child(4) > .nav-link > span:nth-child(1)"));
        urzadzeniaButton.click();
        WebElement telefonyButton = DRIVER.findElement(By.linkText("Telefony"));
        telefonyButton.click();
        destroyDriver();
    }
}
