package com.sda.training;

import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.sda.training.helper.PropertiesHelper.getBrowserName;


public class DriverManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DriverManager.class);

    public static RemoteWebDriver DRIVER;

    public static void startDriver() {
        switch(getBrowserName().toLowerCase()) {
            case "chrome":
                //System.setProperty("webdriver.chrome.driver", "C:\\Users\\thepa\\Desktop\\Pax\\Selenium\\drivers\\chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.setHeadless(true);
                DRIVER = new ChromeDriver(options);
                break;
            case "firefox":
                //System.setProperty("webdriver.gecko.driver", "C:\\Users\\thepa\\Desktop\\Pax\\Selenium\\drivers\\geckodriver.exe");
                ChromeOptions options1 = new ChromeOptions();
                options1.setHeadless(true);
                DRIVER = new FirefoxDriver(options1);
                break;
            default:
                throw new IllegalStateException("Unsupported browser name! Idiot...");
        }
        //DRIVER.manage().window().maximize();
    }


    public static void destroyDriver() {
        DRIVER.quit();
    }

    private static void embedScreenshot(Scenario scenario) {
        byte[] screenshot= DRIVER.getScreenshotAs(OutputType.BYTES);
        scenario.attach(screenshot,"image/png","screenshot");
    }

    private static void saveScreenshotFile(Scenario scenario) throws IOException {
        File srcFile = ((TakesScreenshot) DRIVER).getScreenshotAs(OutputType.FILE);
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        FileUtils.copyFile(srcFile, new File(currentPath + "/sreenshots/" + scenario.getName() + ".png"));
    }

    public static void processExecutedScenario(Scenario scenario) throws IOException {

        String status = (scenario.isFailed() ? "FAILED" : "SUCCES");
        LOGGER.debug("\n================================ SCENARIO FINISH WITH " + status + " STATUS ============\n");

        if(scenario.isFailed() && DRIVER != null) {
            embedScreenshot(scenario);
            saveScreenshotFile(scenario);
        }
        destroyDriver();
        LOGGER.debug("\n=== CLEANUP AFTER TEST ===\n");
    }
}
