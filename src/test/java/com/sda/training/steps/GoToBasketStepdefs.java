package com.sda.training.steps;

import com.sda.training.pages.BasketPage;
import com.sda.training.pages.HomePage;
import com.sda.training.pages.DressesListingPage;
import com.sda.training.pages.ProductCartPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GoToBasketStepdefs {

    HomePage homePage = new HomePage();
    DressesListingPage dressesListingPage = new DressesListingPage();
    ProductCartPage productCartPage = new ProductCartPage();
    BasketPage basketPage = new BasketPage();

    @When("User clicks Dresses in menu")
    public void userClicksDressesInMenu() throws InterruptedException {
        homePage.clickDressesLink();
    }

    @And("In filters user chooses Summer Dresses")
    public void inFiltersUserChoosesSummerDresses() {
        dressesListingPage.setSummerDressesFilter();
    }

    @And("User chooses one on filtered products")
    public void userChoosesOneOnFilteredProducts() {
        dressesListingPage.chooseProduct();
    }

    @And("User change Quantity on {int}")
    public void userChangeQuantityOn(int amountOfItems) {
        productCartPage.setQty(amountOfItems);
    }

    @And("User change Size on L")
    public void userChangeSizeOnL() {
        productCartPage.setSize();
    }

    @And("User change color on different")
    public void userChangeColorOnDifferent() {
        productCartPage.setColor();
    }

    @And("User clicks Add to cart")
    public void userClicksAddToCart() {
        productCartPage.toCart();
    }

    @Then("User is in the cart")
    public void userIsInTheCart() {
        basketPage.makeOrderIsVisible();
    }

    @And("Go to mine site")
    public void goToMineSite() {
        basketPage.clickLogo();
    }
}
