package com.sda.training.steps;

import com.sda.training.pages.AuthenticationPage;
import com.sda.training.pages.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.hamcrest.MatcherAssert.assertThat;

public class AuthenticationStepdefs {

    HomePage homePage = new HomePage();
    AuthenticationPage authenticationPage = new AuthenticationPage();

    @Given("Home page is opened")
    public void homePageIsOpened() {
        homePage.openHomePage();
    }


    @When("User clicks Sign in link in top menu")
    public void userClicksSignInLinkInTopMenu() throws InterruptedException {
        homePage.clickSignInLink();
    }

    @Then("Authentication page is opened")
    public void authenticationPageIsOpened() {
        authenticationPage.verifyIfAuthenticationPageIsOpened();
    }

    @And("User clicks Create an account button")
    public void userClicksCreateAnAccountButton() {
        authenticationPage.clickCreateAccountButton();
    }

    @Then("User gets a message Invalid email address")
    public void userGetsAMessageInvalidEmailAddress() {
        authenticationPage.waitForErrorMessage();

    }

    @Then("User gets a message {string}")
    public void userGetsAMessage(String expectedMessage) {
        authenticationPage.waitForErrorMessage();
        String visibleErrorMessage = authenticationPage.getErrorMessage();
        assertThat("Expected error message should be: " + expectedMessage + " but visible error message is: " + visibleErrorMessage, expectedMessage.equals(visibleErrorMessage));
    }

    @And("User fill email address field with {string}")
    public void userFillEmailAddressFieldWith(String email) {
        authenticationPage.enterEmail(email);
    }
}
