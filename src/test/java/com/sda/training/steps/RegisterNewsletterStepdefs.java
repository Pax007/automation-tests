package com.sda.training.steps;

import com.sda.training.pages.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;

public class RegisterNewsletterStepdefs {

    HomePage homePage = new HomePage();
    Date today = Calendar.getInstance().getTime();


    @When("User click newsletter field")
    public void userClickNewsletterField() {
        homePage.newsletterFieldClickAndClear();
    }

    @Then("User is on home page")
    public void userIsOnHomePage() {
        homePage.bestSellersButtonIsClickable();
    }

    @And("User fill newsletter email address field with {string}")
    public void userFillNewsletterEmailAddressFieldWith(String emailAddress) {
        homePage.enterNewsletterEmail(emailAddress);
    }

    @And("User clicks Enter")
    public void userClicksEnter() {
        homePage.enterClick();
    }

    @Then("User gets a information {string}")
    public void userGetsAInformation(String expectedMessage) {
        homePage.waitForErrorMessage();
        String visibleErrorMessage = homePage.getErrorInfo();
        assertThat("Expected error message should be: " + expectedMessage + " but visible error message is: " + visibleErrorMessage, expectedMessage.equals(visibleErrorMessage));
    }

    @And("User fill email address field with correct {string}")
    public void userFillEmailAddressFieldWithCorrect(String epochTimeMail) throws ParseException {
        SimpleDateFormat crunchifyFormat = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS zzz");
        String currentTime = crunchifyFormat.format(today);
        Date date = crunchifyFormat.parse(currentTime);
        long epochTime = date.getTime();
        String epochTimeSec = String.valueOf(epochTime);
        epochTimeMail = "thepax" + epochTimeSec +"@wp.pl";
        homePage.enterCorrectNewsletterEmail(epochTimeMail);
    }

    @Then("User gets a information {string} about success")
    public void userGetsAInformationAboutSuccess(String informationAboutSuccessRegisterToNews) {
        informationAboutSuccessRegisterToNews = "Newsletter : You have successfully subscribed to this newsletter.";
        homePage.waitForErrorMessage();
        String visibleErrorMessage = homePage.getErrorInfo();
        assertThat("Expected error message should be: " + informationAboutSuccessRegisterToNews + " but visible error message is: " + visibleErrorMessage, informationAboutSuccessRegisterToNews.equals(visibleErrorMessage));
    }
}
