package com.sda.training.steps;

import com.sda.training.pages.HomePage;
import com.sda.training.pages.SearchPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import static org.hamcrest.MatcherAssert.assertThat;

public class SearchStepdefs {

    HomePage homePage = new HomePage();
    SearchPage searchPage = new SearchPage();

    @And("User click search button")
    public void userClickSearchButton() {
        homePage.clickSearchButton();
    }

    @Then("User gets a information on search site {string}")
    public void userGetsAInformationOnSearchSite(String searchResult) {
        searchPage.WaitForResultWithoutResult();
        String visibleErrorMessage = searchPage.GetResultWithoutResult();
        assertThat("Expected error message should be: " + searchResult + " but visible error message is: " + visibleErrorMessage, searchResult.equals(visibleErrorMessage));
    }
}
