package com.sda.training.steps;

import com.sda.training.pages.HomePage;
import io.cucumber.java.en.And;

public class AddToBasketFromMineSiteStepdefs {

    HomePage homePage = new HomePage();

    @And("Add product to cart on mine site")
    public void addProductToCartOnMineSite() {
        homePage.toCartFromMineSite();
    }
}
